#include <stdio.h>

#include <conio.h>
#include <string.h>

#define BUFFER_SIZE (100)

int main()
{
    int priority[BUFFER_SIZE];
    int i = 0, j, k, temp, pnumber;
    char qnames[BUFFER_SIZE][BUFFER_SIZE], names[BUFFER_SIZE][BUFFER_SIZE], buf[BUFFER_SIZE];
    int res = 2;
    char tempChar[BUFFER_SIZE];

    printf("Please enter in the names and priorities for the queue: \n");

    //Get input from user until a blank line is encountered.
    while (res == 2)
    {
        fgets(buf, sizeof(buf), stdin);
        res = sscanf(buf, "%s %d", names[i], &pnumber);
        if(res==2)
        {
           strcpy(qnames[i], names[i]);
           priority[i] = pnumber;
        }
        i++;
    }

    //using a bubble sort sort items according to how they came in and their priority
    for (j = 0 ; j < ( i - 1 ); j++)
    {
        for (k = 0 ; k < i - j - 1; k++)
        {
            if (priority[k + 1] > priority[k])
            {
                temp = priority[k + 1];
                priority[k + 1] = priority[k];
                priority[k] = temp;
                strcpy(tempChar, qnames[k + 1]);
                strcpy(qnames[k + 1], qnames[k]);
                strcpy(qnames[k], tempChar);
            }
        }
    }

    printf("Sorted priority queue:\n");

    //print the sorted queue
    for (j = 0 ; j < i ; j++ )
    {
        printf("%s\n", qnames[j]);
    }

    system("PAUSE");
    return 0;
}
