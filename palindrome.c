#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE (1000)

void findLargestPalindrome(char[], int, int);

int main(int argc, char * argv[])
{
    FILE * fp;
    char buffer[BUFFER_SIZE], stringArray[BUFFER_SIZE];
    int firstPosition = 0, LastPosition;
    char path[BUFFER_SIZE];

    //Prompt user for file
    printf("Please enter a path to a file: \n");
    scanf("%s", path);
    fp = fopen(path, "r");
    if(fp == NULL)
    {
        perror("Error opening file.\n");
        exit(-1);
    }

    //read contents of file into array.
    fscanf(fp, "%s", stringArray);
    while (fscanf(fp,"%s", buffer) != EOF)
    {
      strcat(stringArray, buffer);
    }
    LastPosition = strlen(stringArray);
    //printf("%s\n", buffer);
    printf("%s\n", stringArray);
    findLargestPalindrome(stringArray, firstPosition, LastPosition);
    fclose (fp);
    system("PAUSE");
    return 0;
}

void findLargestPalindrome(char wordToCheck[], int startLetter, int endLetter)
{
    char longerPal[BUFFER_SIZE], challengerPal[BUFFER_SIZE];
    int i, j, k, l, m = 0, first, last;
    int longer = 0, longest = 1;

    for (j = 0; j < endLetter; j++)
    {
        i = j + 1;
        for(k = 0; k < endLetter; k++)
        {
            if(wordToCheck[j] == wordToCheck[i])
            {
                last = i;
                for(first = j; first <= last; first++)
                {
                    if(first == last || first == last - 1)
                    {
                        for(l = j; l <= i; l++)
                        {
                            challengerPal[m]= wordToCheck[l];
                            m++;
                        }
                        m = 0;
                        longer = i - j;
                        if(longer > longest)
                        {
                            longest = longer;
                            for(l = j; l <= i; l++)
                            {
                                longerPal[m] = challengerPal[m];
                                m++;
                            }
                            m = 0;
                        }
                        break;
                    }
                    else if (wordToCheck[first] == wordToCheck[last])
                    {
                        last--;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else
            {
                i++;
            }
        }
    }
    printf("Longest palindrome is: ");
    for(i = 0; i <= longest; i++)
    {
        printf("%c", longerPal[i]);
    }
    printf("\n");
}
